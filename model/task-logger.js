'use strict';

var fs = require('fs');

module.exports.log = function (task) {
    var msg = generateLogMsg(task);
    // console.log(msg);
    fs.appendFile('./server.log', msg, function (err) {
        if (err) { console.error(err); }
    });
};

function generateLogMsg(task) {
    var log = '';
    for (var key in task) {
        log += ('' + task[key] + ' ');
    }
    return log + '\n';
}

