'use strict';

var
  logger = require('./task-logger.js'),
  seqqueue = require('seq-queue'),
  Task = require('./task.js'),
  count = 0,
  manager = {
    queue: seqqueue.createQueue(1000),
    addTask: addTask,
    queueSize: queueSize,
    queueTaskTotalSize: 0
  };

function addTask(req) {
  var task = Task.create(req, queueSize());
  manager.queueTaskTotalSize += task.size;
  var wrapper = wrapTask(task);
  manager.queue.push(wrapper, taskTimeout, 60000);
}

function wrapTask(taskInfo) {
  taskInfo.enter = new Date().getTime();
  return function (task) {
    manager.queueTaskTotalSize -= taskInfo.size;
    taskInfo.start = new Date().getTime();
    var delay = taskInfo.size / 1000;
    setTimeout(function () {
      taskInfo.end = new Date().getTime();
      logger.log(taskInfo);
      task.done();
    }, delay);
  }
}

function queueSize() {
  return manager.queue.queue.length;
}

function taskTimeout() {
  console.error('task timeout');
}

module.exports.attachTo = function (server) {
  server.manager = manager;
};
