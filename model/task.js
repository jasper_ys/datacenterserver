'use strict';


module.exports.create = function (req, size) {
  var task = parseRequest(req);
  task.queueLen = size;
  return task
};

function parseRequest(request) {
  var parts = request.body.data.split(' ');
  return {
    trace: parts[0],
    block: parseInt(parts[1]),
    partition: parseInt(parts[2]),
    size: parseInt(parts[3]),
    create: parseFloat(parts[4]),
    calculated: parseFloat(parts[5]),
    controller: parseInt(parts[6]),
    index: parseInt(parts[7])
  };
}

