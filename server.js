'use strict';

var
  express = require('express'),
  methodOverride = require('method-override'),
  bodyParser = require('body-parser'),
  manager = require('./model/task-manager.js'),

  port = parseInt(process.argv[2]) || 3000,
  server = express(),
  apiBase = './api/',
  apis = [
    'task.js',
    'queue.js'
  ];

server.use(express.static('./static/'));
server.use(methodOverride());
server.use(bodyParser.urlencoded({ extended: false }));
server.use(bodyParser.json());

// task manager
manager.attachTo(server);

// apis
for (var i = 0; i < apis.length; i++) {
  var api = require(apiBase + apis[i]);
  api.attachTo(server);
}

server.listen(port);
console.log('\x1b[32mHTTP Server starts listening on %d ...\x1b[0m', port);
