'use strict';

module.exports.attachTo = function (server) {

  var
    manager = server.manager,
    route = '/task';

  server.post(route, function (req, res, next) {
    manager.addTask(req);
    res.status(200).send({
      tid: req.body.tid || 0
    });
  });

};
