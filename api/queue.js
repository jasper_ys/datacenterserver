'use strict';

module.exports.attachTo = function (server) {

  var
    manager = server.manager,
    route = '/queue';

  server.get(route, function (req, res, next) {
    res.status(200).send({
      size: manager.queueTaskTotalSize
    });
  });
};
